/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "-swname-"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "ik5nax@radioteknos.it"

/* Define to the full name of this package. */
#define PACKAGE_NAME "<swname>"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "<swname> __VERSION"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "-swname-"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "__VERSION"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "__VERSION"

/* "current version" */
#define __VERSION "0.1"

/* "current version date" */
#define __VERSION_DATE "30/05/2020"

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */

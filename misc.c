/*
 *   naxrot2d - NAXROT2 "driver/daemon"
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdio.h>       
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
/* #include <asm/termios.h> */
#include <sys/ioctl.h>
#include <signal.h>
#include <termios.h>
#include <fcntl.h>
#include "main.h"
#include "misc.h"


struct b2tt{
  int baudrate;
  int termiosrate;
} baud2termiostable[]={
		       /* Posix */
		       {0,      B0}, /* hang up */
		       {50,     B50},
		       {75,     B75},
		       {110,    B110},
		       {134,    B134},
		       {150,    B150},
		       {200,    B200},
		       {300,    B300},
		       {600,    B600},
		       {1200,   B1200},
		       {1800,   B1800},
		       {2400,   B2400},
		       {4800,   B4800},
		       {9600,   B9600},
		       {19200,  B19200},
		       {38400,  B38400},
		       /* not-Posix*/ 
		       {57600,    B57600},
		       {115200,   B115200},
		       {230400,   B230400},
		       {460800,   B460800},
		       {500000,   B500000},
		       {576000,   B576000},
		       {921600,   B921600}, 
		       {1000000,  B1000000},
		       {1152000,  B1152000},
		       {1500000,  B1500000},
		       {2000000,  B2000000},
		       {2500000,  B2500000},
		       {3000000,  B3000000},
		       {3500000,  B3500000},
		       {4000000 ,  B4000000}
		       /* #define __MAX_BAUD B4000000 */
};


extern int verbose, fdser;

unsigned int skipspace(char **b){
  while(((*b)[0]==' ' || (*b)[0]=='\t') && (*b)[0]!='\0') 
    (*b)++;
  if(*b[0]=='\0')
    return(1);
  else
    return(0);
}

unsigned int skipnotspace(char **b){
  while(*b[0]!=' ' && *b[0]!='\t' && *b[0]!='\0')
    (*b)++;
  if(*b[0]=='\0')
    return(1);
  else
    return(0);
}

int cbmsgpush(char *cb, char *msg, int *wp, int *rp,
	      const int msglen, const int cbnummsg){
  
  strcpy(cb+*wp*msglen, msg);

  (*wp)++;
  if(*wp>=cbnummsg)
    *wp=0;
  if(*wp==*rp)
    return(-1);
  else
    return(0);
}

int cbmsgpop(char *cb, char *msg, int *wp, int *rp,
	      const int msglen, const int cbnummsg){

  if(*rp!=*wp){
    strcpy(msg, cb+*rp*msglen);
    
    (*rp)++;
    if(*rp>=cbnummsg)
      *rp=0;
    
    return(0);
  }
  else{
    return(-1); /* no msg to be poped */
  }
}

int sercbmsgpush(char *cb, char *msg, int *wp, int *rp){
  int i;

  for (i=0; i<(int)strlen(msg); i++){
    cb[*wp]=msg[i];
    *wp=(*wp)+1;
    if(*wp>=SERCBLEN)
      *wp=0;
    /* TODO: handle overwrite */
  }
  
  if(*wp==*rp)
    return -1;
  else
    return 0;
}

int sercbmsgpop(char *cb, char *msg, int *wp, int *rp){
  int i, lrp;
  
  if(*rp!=*wp){
    lrp=*rp;
    i=0;
    while(lrp!=*wp){
      msg[i]=cb[lrp];
      i++;
      lrp++;
      if(lrp>=SERCBLEN)
	lrp=0;
      if(cb[lrp]=='\n'){ /* TODO: add \r, too? */
	lrp++;
	if(lrp>=SERCBLEN)
	  lrp=0;	
	*rp=lrp;
	msg[i]='\0';
	return 0;
      }
    }
    return -1;
  }
  else{
    return -1; /* no msg to be poped */
  }
}

int baud2termios(int baudrate){
  unsigned int i;
  
  for(i=0; i<(int)sizeof(baud2termiostable)/
	sizeof(baud2termiostable[0]); i++){
    if(baud2termiostable[i].baudrate==baudrate){
      return baud2termiostable[i].termiosrate;
    }
  }

  return -1; /* invalid baud rate */
}

int serial_open(char *sfn, int speed){
int fd;
struct termios sersettings;

 if((fd=open(sfn, O_RDWR|O_NOCTTY|O_NDELAY))!=-1) {
   bzero(&sersettings, sizeof(sersettings));

   
   sersettings.c_cflag=baud2termios(speed)|CS8|CLOCAL|CREAD;
   sersettings.c_iflag=IGNPAR | IXON | IXOFF |IGNBRK;
   sersettings.c_oflag=0;
   sersettings.c_lflag=ICANON;
   sersettings.c_cc[VTIME]=0;
   sersettings.c_cc[VMIN]=0;
   tcflush(fd, TCIOFLUSH);
   tcsetattr(fd, TCSANOW, &sersettings);
   tcflush(fdser, TCIOFLUSH);                /* flush rx and tx buffer */
 }

 return fd;
}

int serial_send(char *b){
  int i;

  if(verbose>0)
    printf("serial send: ");
  for(i=0; i<(int)strlen(b)+1; i++) {
    write(fdser, b+i, 1);
    if(verbose>0)
      printf("%c", b[i]);
    usleep(2000); /* TODO: adj */
  }
  if(verbose>0)
    printf("\n");
  
  return(0);
}

void naxrot2msgcleanup(char *buff, int n){
  char tmpbuff[BUFFLEN];
  int i, j;
  
  strncpy(tmpbuff, buff, BUFFLEN-1);
  
  j=0;
  for(i=0; i<n; i++)
    if(tmpbuff[i]!=0x00 &&
       tmpbuff[i]!=0x11 &&
       tmpbuff[i]!=0x13 &&
       tmpbuff[i]!=0x0d){
      buff[j]=tmpbuff[i];
      j++;
    }      
  buff[j]='\0';
  
}


void use(void){
  
  printf("naxrot2d usage:\n\n");
  printf("  naxrot2d [-h] [-v] [-p <serialportname>] [-a <ip_addr>]\n");
  printf("  -h (this) help\n");
  printf("  -v verbose, print some debugging msg\n");
  printf("  -p <serialportname> specify serial port to which NAXROT2 is attached\n");
  printf("     (default /dev/ttyS0)\n");
  printf("  -a <ip_addr> specify ip address where rotctrlp is running (default 127.0.0.1)\n");
  printf("  -s <speed> serial port speed (bit/s) - default 9600 bit/s\n");
  printf("\n");
}




int cfg_get(float *max_el, char *serialportname, int *serialspeed){
  int l=1;
  char homedirname[40], cfgdir[40], gf[80], linebuff[80], *pb;
  char section[40];
  FILE *fpcfg;

  /* cfg dir is on;y $HOME/.trk to be modified also in trk (TODO) */
  strcpy(homedirname, getenv("HOME"));
  if(homedirname==NULL){
    printf("Unable to find HOME dir (?!)\n");
    return -1;
  }
  strcat(homedirname, "/");
  strcpy(cfgdir, homedirname); strcat(cfgdir, trkdirname);
  if(access(cfgdir, F_OK)==-1) {
    printf("Unable to find a \".trk\" dir neither in current nor in $HOME\
 dir!\n");
    return -1;
  }   
  
  /* searchv for naxrot2d configuration file */
  strcpy(gf, cfgdir);
  strcat(gf, rotdcfgname);
  if(access(gf, R_OK)==-1){
    fprintf(stderr, "Unable to find %s in %s\n", rotdcfgname, cfgdir);
    return -1; }
  
  /* try to open cfg file */
  if((fpcfg=fopen(gf, "r"))==NULL){
    fprintf(stderr, "Unable to open %s\n", rotdcfgname);
    return -1;
  }
  /* and get values */
  while(l){
    if(fgets(linebuff, 78, fpcfg)==NULL){
      l=0;
      continue;
    }
    
    /* flush off spaces and tabs */
    pb=(char*)linebuff;
    skipspace(&pb);
    /* strip <CR> <LF> */
    pb[strcspn(pb, "\r\n")]='\0';

    /* # is for comments */
    if(pb[0]=='#' || strlen(pb)==0)
      continue;

    /* = sections ident = */
    if(pb[0]=='['){
      if(strncasecmp(pb+1, "serial", strlen("serial"))==0){
	strcpy(section, "serial");
      }
      else if(strncasecmp(pb+1, "maxel", strlen("maxel"))==0){
	strcpy(section, "maxel");
      }
    }
    /* # sections ident end # */

    /* = sections scan = */
    else{
      /* = [serial] section scan = */
      if(strcmp(section, "serial")==0){
	if(strncasecmp(pb, "port", strlen("port"))==0){
	  if(sscanf(pb+strlen("port")+1, "%s", serialportname)==0){
	    fprintf(stderr, "wrong value for port in %s\n", rotdcfgname);
	    return -1;
	  }
	}
	else if(strncasecmp(pb, "speed", strlen("speed"))==0){
	  if(sscanf(pb+strlen("speed")+1, "%d", serialspeed)==0){
	    fprintf(stderr, "wrong value for speed in %s\n", rotdcfgname);
	    return -1;
	  }
	}

      } /* # [serial] section end # */
      
      /* = [maxel] section scan = */
      else if(strcmp(section, "maxel")==0){
	if(strncasecmp(pb, "max_el", strlen("max_el"))==0){
	  if(sscanf(pb+strlen("max_el")+1, "%f", max_el)==0){
	    fprintf(stderr, "wrong value for max_el in %s\n", rotdcfgname);
	    return -1;
	  }
	}
	else{
	  fprintf(stderr, "wrong field %s in %s section\n", pb, section);
	  return -1;
	}
      } /* # [maxel] section end # */

    } /* # sections scan end # */
    
  } /* # rotdcfgname scan end # */
  
  
  return 0;
}

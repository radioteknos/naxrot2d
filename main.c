/*
 *   naxrot2d - NAXROT2 "driver/daemon"
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdio.h>       
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include "misc.h"
#include "daemon.h"
#include "main.h"



int ml=1, verbose, fdser;

int main(int argc, char **argv){
  int clo, rv, n, aereqpresc;
  int sfdcmd, sfdans;
  char buff[BUFFLEN], *pb; 
  char serialportname[BUFFLEN]="/dev/ttyS0",
    rotctrlp_addr[BUFFLEN]="127.0.0.1";
  int serialspeed=9600;
  int cmdch_port=1300, ansch_port=1301;
  struct sockaddr_in cmdch, ansch; 
  socklen_t ansch_addr_size;
  char msgbuff[BUFFLEN];
  int lastfd=0;
  fd_set rfds;
  struct timeval timeout;
  float val;
  int reqwp, reqrp;
  char reqcb[REQCBLEN];
  int serqueuestat, attempt, ackto;
  char reqmsg[REQMSGLEN], backmsg[BACKMSGLEN];
  float max_el;
  int serwp, serrp;
  char sercb[REQCBLEN];
  
  /* catch ctrl+C */
  signal(SIGINT, quit);

  /* variable init */
  serqueuestat=SERQUEUEIDLE;
  attempt=0;
  ackto=0;
  verbose=0;
  reqwp=0;
  reqrp=0;
  serwp=0;
  serrp=0;
  
  /* read cfg file and init variable */
  rv=cfg_get(&max_el, serialportname, &serialspeed);
  if(rv!=0)
    return rv;
  
  /* cmd line scan */
  while((clo=getopt(argc, argv, "vhp:a:s:"))!=-1){
    switch(clo){
    case 'v':
      verbose=1;
      break;
    case 'h':
      use();
      return(0);
      break;
    case 'p':
      if(strlen(optarg)>BUFFLEN-1){
	printf("serial port name too long!\n");
	return(1);
      }
      if(sscanf(optarg, "%s", serialportname)==0 ) { 
	printf("invalid serial port name\n");
	return(1);
      } break; 
    case 'a':
      if(strlen(optarg)>BUFFLEN-1){
	printf("ip address string too long!\n");
	return(1);
      }
      if(sscanf(optarg, "%s", rotctrlp_addr)==0 ) { 
	printf("invalid ip address\n");
	return(1);
      } break; 
    case 's':
      if(strlen(optarg)>BUFFLEN-1){
	printf("serial speed too long!\n");
	return(1);
      }
      if(sscanf(optarg, "%d", &serialspeed)==0 ) { 
	printf("invalid speed\n");
	return(1);
      } break; 
    default:
      return(1);
    }
  }


 /* try to make a daemon */
  if(verbose==0){
    if(daemon_start(1)==0) {
      fprintf(stderr, "naxrot2d cannot beacame a daemon\n");
      return 1;
    }
  }
  else {
    printf("in verbose mode naxrot2d cannot be a daemon\n");
  }
  
  
  /* open serial port with NAXROT2 */  
  if((fdser=serial_open(serialportname, serialspeed))==-1){
    fprintf(stderr, "Unable to open serial port %s\n",
	    serialportname);
    return(1);
  }

  /* socket init */
  sfdans=socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0); 
  ansch.sin_family=AF_INET;
  ansch.sin_port=htons(ansch_port);
  ansch.sin_addr.s_addr=inet_addr(rotctrlp_addr);
  memset(ansch.sin_zero, '\0', sizeof(ansch.sin_zero));
  ansch_addr_size=sizeof(ansch);


  memset(&cmdch, 0, sizeof(cmdch));
  cmdch.sin_family=AF_INET; 
  cmdch.sin_addr.s_addr=htonl(INADDR_ANY);
  cmdch.sin_port=htons(cmdch_port);

  sfdcmd=socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
  if(sfdcmd==-1){
   fprintf(stderr, "unable to create socket on port %d\n", cmdch_port);
   return(1);
  }

  rv=bind(sfdcmd, (struct sockaddr *)&cmdch, sizeof(cmdch));
  if(rv==-1){
    fprintf(stderr, "unable to bind port %d to addr %s\n",
	    cmdch_port, inet_ntoa(cmdch.sin_addr));
    return(1);
  }

  
  /* select() setup */
  if(lastfd<sfdcmd)
    lastfd=sfdcmd;
  if(lastfd<fdser)
    lastfd=fdser;
  FD_ZERO(&rfds);
  FD_SET(sfdcmd, &rfds);
  FD_SET(fdser, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=SELECTTO;


  aereqpresc=AEREQPRESC;
  while(ml){
    rv=select(lastfd, &rfds, NULL, NULL, &timeout);
    switch(rv){

    case -1:                                                     /* error */
      /* TODO: handle errors? */
      if(ml==1){
	printf("select err\n");
	timeout.tv_sec=0;
	timeout.tv_usec=SELECTTO;
	FD_ZERO(&rfds);
	FD_SET(sfdcmd, &rfds);
	FD_SET(fdser, &rfds);
      }
      break;
      
    case 0:                               /* timeout */
      switch(serqueuestat){
      case SERQUEUEIDLE:
	rv=cbmsgpop(reqcb, buff, &reqwp, &reqrp,
		    REQMSGLEN, REQCBNUMMSG);
	if(rv==0){
	  strncpy(reqmsg, buff, REQMSGLEN-1);
	  reqmsg[REQMSGLEN-1]='\0';
	  
	  serial_send(reqmsg);
	  backmsg[0]='\0';
	  
	  attempt=ATTEMPTNUM;
	  serqueuestat=SERQUEUERACK;
	}
	else{
	  /* periodic request AZ/EL (prescaled) */
	  if(aereqpresc==AEREQPRESC/2){
	    sprintf(msgbuff, "%s\n", GETAZ);
	    serial_send(msgbuff);
	  }
	  if(aereqpresc==0){
	    sprintf(msgbuff, "%s\n", GETEL);
	    serial_send(msgbuff);	    
	    aereqpresc=AEREQPRESC;
	  }
	  else
	    aereqpresc--;
	  
	  /* check for backmsg[] not empty and send to rotctrlp */
	  if(strlen(backmsg)!=0){
	    if(verbose>0){
	      printf("got from NAXROT2: %s\n", backmsg);
	    }
	    switch(toupper(backmsg[0])){
	    case 'A':
	      sscanf(backmsg+1, "%f", &val);
	      if(verbose>0)
		printf("A %5.1f\n", val);
	      sprintf(msgbuff, "%s %5.1f", cmdBACKAZ, val);	    
	      sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		     (struct sockaddr *)&ansch, ansch_addr_size);	      
	      break;
	    case 'E':
	      sscanf(backmsg+1, "%f", &val);
	      if(verbose>0)
		printf("E %5.1f\n", val);
	      sprintf(msgbuff, "%s %5.1f", cmdBACKEL, val);	    
	      sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		     (struct sockaddr *)&ansch, ansch_addr_size);	      
	      break;
	    case 'N':
	      sscanf(backmsg+1, "%d", &rv);
	      if(verbose>0)
		printf("Error %d\n", rv);
	      sprintf(msgbuff, "%s %d", cmdERROR, rv);	    
	      sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		     (struct sockaddr *)&ansch, ansch_addr_size);
	      break;
	      /* TODO: complete for other backmsg */
	    }
	  }
	  backmsg[0]='\0'; 
	}
	break;
      case SERQUEUERACK:
	if(attempt>0){
	  switch(reqmsg[0]){
	  case 'a':
	    sprintf(msgbuff, "%s\n", LASTAZ);
	    serial_send(msgbuff);
	    break;
	  case 'e':
	    sprintf(msgbuff, "%s\n", LASTEL);
	    serial_send(msgbuff);
	    break;
	    /* TODO: complete with other cmd and error */
	  }
	    
	  backmsg[0]='\0';
	  attempt--;
	  serqueuestat=SERQUEUEWACK;
	  ackto=ACKTO;
	}
	else
	  serqueuestat=SERQUEUENACK;	  
	break;
      case SERQUEUEWACK:
	if(strlen(backmsg)==0){
	  if(ackto!=0)
	    ackto--;
	  else
	    serqueuestat=SERQUEUERACK;
	}
	else{
	  if(verbose>0)
	    printf("received from rotor: %s\n", backmsg);

	  /* TODO: clenaup received msg (skipspace, etc...) */

	  if(toupper(backmsg[0])=='I'){
	    sscanf(backmsg+1, "%f", &val);
	    if(verbose>0)
	      printf("%s %5.1f\n", cmdLASTAZ, val);
	    sprintf(msgbuff, "%s %5.1f", cmdLASTAZ, val);	    
	    sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		   (struct sockaddr *)&ansch, ansch_addr_size);

	  }
	  else if(toupper(backmsg[0])=='J'){
	    sscanf(backmsg+1, "%f", &val);
	    if(verbose>0)
	      printf("%s %5.1f\n", cmdLASTEL, val);
	    sprintf(msgbuff, "%s %5.1f", cmdLASTEL, val);	    
	    sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		   (struct sockaddr *)&ansch, ansch_addr_size);
	  }
	  else if(toupper(backmsg[0])=='A'){
	    sscanf(backmsg+1, "%f", &val);
	    if(verbose>0)
	      printf("%s %5.1f\n", cmdBACKAZ, val);
	    sprintf(msgbuff, "%s %5.1f", cmdBACKAZ, val);	    
	    sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		   (struct sockaddr *)&ansch, ansch_addr_size);

	  }
	  else if(toupper(backmsg[0])=='E'){
	    sscanf(backmsg+1, "%f", &val);
	    if(verbose>0)
	      printf("%s %5.1f\n", cmdBACKEL, val);
	    sprintf(msgbuff, "%s %5.1f", cmdBACKEL, val);	    
	    sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		   (struct sockaddr *)&ansch, ansch_addr_size);
	  }
	  /* TODO: this is for test; if naxrot2 answer is N* resend req */
	  /* really this could be mantained and add 
	     ackto=0;
	     serqueuestat=SERQUEUERACK;
	  */
	  else if(toupper(backmsg[0])=='N'){
	    sscanf(backmsg+1, "%d", &rv);
	    if(verbose>0)
	      printf("Error %d\n", rv);
	    sprintf(msgbuff, "%s%d", cmdERROR, rv);	    
	    sendto(sfdans, msgbuff, strlen(msgbuff), 0,
		   (struct sockaddr *)&ansch, ansch_addr_size);
	  }

	  else{
	    ackto=0;
	    serqueuestat=SERQUEUERACK;
	  }

	  /* TODO: better goto IDLE only i/j is just what is expected */
	  serqueuestat=SERQUEUEIDLE;	  
	}
	break;
      case SERQUEUENACK:
	printf("no answ!\n"); /* TODO: better */
	  serqueuestat=SERQUEUEIDLE;	  
	break;
      case SERQUEUEERR:
	break;
      case SERQUEUERES:
	break;
      default:
	printf("internal error: serqueuestat=%d\n", serqueuestat);
	serqueuestat=SERQUEUEIDLE;
      }

      timeout.tv_sec=0;
      timeout.tv_usec=SELECTTO;
      FD_ZERO(&rfds);
      FD_SET(sfdcmd, &rfds);
      FD_SET(fdser, &rfds);
      break;

    default:                                                /* fd has data */
      if(FD_ISSET(sfdcmd, &rfds)){                        /* cmdch socket */
	
	n=recv(sfdcmd, buff, BUFFLEN-1, 0);
	buff[n]='\0';

	/* TODO: remove */
	strcpy(msgbuff, "[]: "); strcat(msgbuff, buff);
		
	/* = cmd decode = */
	/* flush off spaces and tabs */
	pb=(char*)buff;
	skipspace(&pb);
	/* strip <CR> <LF> */
	pb[strcspn(pb, "\r\n")]='\0';
	
	/* = scan cmds = */
	if(strlen(pb)!=0){

	  if(strncasecmp(pb, cmdSETAZ, n=strlen(cmdSETAZ))==0){
	    if(verbose>0)
	      printf("cmd_az_set: %s\n", pb);
	    pb+=n;	    
	    if(sscanf(pb, "%f", &val)==0){
	      fprintf(stderr, "bad value for AZ: %s\n", pb);
	    }
	    else{
	      sprintf(buff, "a %5.1f\n", val);
	      cbmsgpush(reqcb, buff, &reqwp, &reqrp,
			REQMSGLEN, REQCBNUMMSG);
	    }
	  }

	  else if(strncasecmp(pb, cmdSETEL, n=strlen(cmdSETEL))==0){
	    if(verbose>0)
	      printf("cmd_el_set: %s\n", pb);
	    pb+=n;	    
	    if(sscanf(pb, "%f", &val)==0){
	      fprintf(stderr, "bad value for EL: %s\n", pb);
	    }
	    else{
	      if(val>max_el)
		val=max_el;
	      sprintf(buff, "e %5.1f\n", val);
	      cbmsgpush(reqcb, buff, &reqwp, &reqrp,
			REQMSGLEN, REQCBNUMMSG);
	    }

	  }

	  else if(strncasecmp(pb, cmdQUIT, n=strlen(cmdQUIT))==0){
	    if(verbose>0)
	      printf("received request to quit (%s)\n", pb);
	    ml=0;
	  }

	  else{
	    fprintf(stderr, "\n[unk:]%s\n", pb);
	  }
	} /* # cmd decode end # */
	

	
	/* TODO: remove and send only on request for those cmd who as ans */
	/* sendto(sfdans, msgbuff, strlen(msgbuff) , 0, */
	/*        (struct sockaddr *)&client, /\* sizeof(client) *\/l); */
	sendto(sfdans, msgbuff, strlen(msgbuff), 0,
  	 (struct sockaddr *)&ansch, ansch_addr_size);
	if(verbose>0)
	  printf("[send]: %s\n", msgbuff);
	
	/* TODO: better */
	/* if(verbose>0) */
	/*   printf("%s\n",buff);  */
	
      } /* */
      if(FD_ISSET(fdser, &rfds)){                        /* serial port */
	n=read(fdser, buff, BUFFLEN-1);
	
	naxrot2msgcleanup(buff, n);

	sercbmsgpush(sercb, buff, &serwp, &serrp);
	if(verbose>0){
	  fprintf(stderr, "DATA from serial line %s: %d bytes\n",
		  serialportname, n);
	  printf("%s\n", buff);
	  /* leave this for extreme debug */
	  /* for(i=0; i<24; i++) */
	  /*   printf("%02x ", buff[i]); */
	  /* printf("\n"); */
	}

	if(sercbmsgpop(sercb, buff, &serwp, &serrp)==0){
	  /* printf("! %s\n", buff); */
	  
	  /* strip <CR> <LF> */
	  buff[strcspn(buff, "\r\n")]='\0';	
	  strncpy(backmsg, buff, BACKMSGLEN-1);
	  backmsg[BACKMSGLEN-1]='\0';
	}

      }
	
      timeout.tv_sec=0;
      timeout.tv_usec=SELECTTO;
      FD_ZERO(&rfds);
      FD_SET(sfdcmd, &rfds);
      FD_SET(fdser, &rfds);
    } /* # select switch end # */

  } /* # main loop end # */
 
  fprintf(stderr,"\nnaxrot2d terminated [PID %d]\n\n", getpid());
  return(0);
}


void quit(int signo){
  if(signo==SIGINT){    
    ml=0;
  }
}

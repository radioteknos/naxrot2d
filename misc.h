/*
 *   naxrot2d - NAXROT2 "driver/daemon"
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define trkdirname     ".trk/"
#define rotdcfgname     "rotor.cfg"

unsigned int skipspace(char **b), skipnotspace(char **b);
int cbmsgpush(char *cb, char *msg, int *wp, int *rp,
	      const int msglen, const int cbnummsg),
  cbmsgpop(char *cb, char *msg, int *wp, int *rp,
	      const int msglen, const int cbnummsg);
void use(void);
int serial_open(char *sfn, int speed), serial_send(char *b),
  baud2termios(int baudrate);
void naxrot2msgcleanup(char *buff, int n);
int cfg_get(float *max_el, char *serialportname, int *serialspeed);
int sercbmsgpush(char *cb, char *msg, int *wp, int *rp),
  sercbmsgpop(char *cb, char *msg, int *wp, int *rp);

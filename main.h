/*
 *   naxrot2d - NAXROT2 "driver/daemone"
 * 
 *      (c) by Lapo Pieri 2020
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define SELECTTO 100000  /* 100ms */

#define cmdSETAZ  "AZ"
#define cmdSETEL  "EL"

#define REQMSGLEN    16
#define REQCBNUMMSG  8
#define REQCBLEN (REQMSGLEN*REQCBNUMMSG)
#define BACKMSGLEN   32

#define SERCBLEN 256

#define BUFFLEN 128

#define ATTEMPTNUM 3
#define ACKTO 3

#define AEREQPRESC 20 /* prescaler to select() timeout to send a/e req */

/* set cmd (has param) */
#define SETAZ       "a %5.1f"
#define SETEL       "e %5.1f"
#define SETHIST     "h %4.1f"

/* get cmd (no param) */
#define LASTAZ      "i"
#define LASTEL      "j"
#define LASTHIST    "h"
#define GETAZ       "a"
#define GETEL       "e"
#define GETSTAT     "s"

/* 
   cmd names meaning:

   "SET" set a value
   "BACK" ask a value that can vary
   "LAST" ask the last settled value (that cannot vary)

*/
/* rotctrlp <-> naxrot2d */
#define cmdSETAZ      "AZ"            /* 'a' */
#define cmdBACKAZ     "AZ"            /* 'a' */
#define cmdLASTAZ     "LASTAZ"        /* 'i' */
#define cmdSETOFFAZ   "OFFAZ"         /* 'f' */
#define cmdLASTOFFAZ  "LASTOFFAZ"     /* 'f' */
#define cmdSETEL      "EL"            /* 'e' */
#define cmdBACKEL     "EL"            /* 'e' */
#define cmdLASTEL     "LASTEL"        /* 'j' */
#define cmdSETOFFEL   "OFFEL"         /* 'g' */
#define cmdLASTOFFEL  "LASTOFFEL"     /* 'g' */
#define cmdERROR      "ERROR"         /* 'n' */
#define cmdSETHIST    "HIST"          /* 'h' */
#define cmdLASTHIST   "LASTHIST"      /* 'h' */
#define cmdSETSTEP    "STEP"          /* 'p' */
#define cmdLASTSTEP   "LASTSTEP"      /* 'p' */
#define cmdCCW        "CCW"           /* 'l' */
#define cmdCW         "CW"            /* 'r' */
#define cmpUP         "UP"            /* 'u' */
#define cmdDOWN       "DOWN"          /* 'd' */
#define cmdQUIT       "QUIT"          /* request to quit naxrot2d */


#define SERQUEUEIDLE  0  /* idle */
#define SERQUEUERACK  1  /* ack has to be requested (waiting for i/j ans) */
#define SERQUEUEWACK  2  /* waiting for ack */
#define SERQUEUENACK  3  /* ack timeout (not received) */
#define SERQUEUEERR   4  /* got error */
#define SERQUEUERES   5  /* got reset */

void quit(int signo);
